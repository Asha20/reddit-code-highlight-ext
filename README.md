# Reddit Code Highlight

This extension will automatically highlight code blocks on Reddit. The extension will use the name of the subreddit you're in, as well as the code itself to try and guess what language it's written in, before highlighting it using PrismJS.

*Post before being highlighted*
![Code blocks before highlighting](.gitlab/before-highlighting.png)

*Post after being highlighted*
![Code block after highlighting](.gitlab/after-highlighting.png)

## What this extension does

 - Adds line numbers to the left side of the code
 - Allows you to choose a theme from a popup window (8 default themes that come with PrismJS are provided; the one used in the example screenshot is *Coy*)
 - Tries to automatically guess the language inside the code block
 - Provides a dropdown with all of the supported languages, in case you wanted to change it
 - Provides a quick set of suggestion buttons next to the language dropdown. For example, if you're writing in a subreddit related to web development, the suggestion buttons offer a quick way of accessing most commonly used webdev languages, instead of needing to search through the dropdown.

## Contributing

There are two different ways to contribute to this extension:

### Improving the extension's automatic language detection feature
 
This extension has the capability to highlight [all of the languages supported by PrismJS.](https://prismjs.com/#languages-list) Since I'm only familiar with web development and PrismJS supports 151 languages at this time, it's hard for me to track down all of the appropriate subreddits for those languages, as well as appropriate suggestions that the extension should offer.

### Fixing bugs, adding features...

The other type of contributing is just regular bugfixing and improving the extension outside of the language detection.

---

For installing and contributing instructions, take a look at [`CONTRIBUTING.md`](CONTRIBUTING.md).

## License

MIT license; check out the [`LICENSE`](LICENSE) file.