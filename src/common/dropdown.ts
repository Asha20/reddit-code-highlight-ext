type Handler<T> = (item: T) => void;

export class Dropdown<T> {
  private select: HTMLSelectElement;
  private handler: Handler<T> | undefined;

  constructor(private items: T[], private idProp: keyof T) {
    this.select = document.createElement("select");

    items.forEach((item, i) => {
      const option = document.createElement("option");
      option.value = String(i);
      option.textContent = (item as any)[idProp];
      this.select.appendChild(option);
    });
  }

  getDOM() {
    return this.select;
  }

  getSelected() {
    return this.items[this.select.selectedIndex];
  }

  setSelected(wanted: T) {
    const index = Math.max(0, this.items.findIndex(item => {
      return (item as any)[this.idProp] === (wanted as any)[this.idProp];
    }));
    if (index === this.select.selectedIndex) {
      return;
    }
    this.select.selectedIndex = index;
    if (this.handler) {
      this.handler(this.items[index]);
    }
  }

  onChange(handler: Handler<T>) {
    if (this.handler) {
      this.select.removeEventListener("change", this.handler as any);
    }
    this.handler = handler;
    this.select.addEventListener("change", () => {
      const selected = this.items[this.select.selectedIndex];
      handler(selected);
    });
  }
}
