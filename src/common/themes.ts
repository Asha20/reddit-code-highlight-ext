export interface Theme {
  name: string;
  display: string;
}

export const themes: Theme[] = [
  {name: "default", display: "Default"},
  {name: "dark", display: "Dark"},
  {name: "funky", display: "Funky"},
  {name: "okaidia", display: "Okaidia"},
  {name: "twilight", display: "Twilight"},
  {name: "coy", display: "Coy"},
  {name: "solarizedlight", display: "Solarized Light"},
  {name: "tomorrow", display: "Tomorrow Night"},
];

export const defaultTheme = themes[0];
