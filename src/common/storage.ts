import {Theme, defaultTheme} from "./themes";

export function get(obj: any): Promise<any> {
  return new Promise(resolve => {
    chrome.storage.local.get(obj, resolve);
  });
}

export function set(obj: any): Promise<any> {
  return new Promise(resolve => {
    chrome.storage.local.set(obj, resolve);
  });
}

export function getTheme(): Promise<Theme> {
  return get({theme: defaultTheme})
    .then(result => result.theme);
}

export function setTheme(theme: Theme): Promise<void> {
  return set({theme});
}
