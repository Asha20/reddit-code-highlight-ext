import * as Storage from "../common/storage";
import {Dropdown} from "../common/dropdown";
import {Theme, defaultTheme, themes} from "../common/themes";
// tslint:disable-next-line no-var-requires
const pkg = require("../../package.json");

function updateTheme(theme: Theme = defaultTheme) {
  chrome.tabs.query({active: true, currentWindow: true}, tabs => {
    chrome.tabs.sendMessage(tabs[0].id!, {
      type: "UPDATE_THEME",
      theme,
    });
  });
}

const dropdown = new Dropdown(themes, "display");
dropdown.onChange(updateTheme);
Storage.getTheme().then(theme => dropdown.setSelected(theme));

const destination = document.querySelector(".theme-dropdown")!;
destination.parentNode!.replaceChild(dropdown.getDOM(), destination);

const repoInfo = {
  version: pkg.version,
  repository: pkg.repository && pkg.repository.url,
};

(Array.from(document.querySelectorAll("[data-external]")) as HTMLAnchorElement[])
  .forEach(a => {
    a.addEventListener("click", e => {
      e.preventDefault();
      chrome.tabs.create({active: true, url: a.href});
    });
  });

(Array.from(document.querySelectorAll("[data-pkg]")) as HTMLElement[])
  .forEach(el => {
    const key = el.dataset.pkg as keyof typeof repoInfo;
    if (repoInfo[key]) {
      if (key === "repository") {
        (el as HTMLAnchorElement).href = repoInfo[key];
        return;
      }
      el.textContent = repoInfo[key];
    }
});
