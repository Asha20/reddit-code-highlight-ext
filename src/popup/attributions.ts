Array.from(document.querySelectorAll("pre[data-dependency-name]"))
  .forEach(pre => {
    const name = pre.getAttribute("data-dependency-name")!;
    const license = chrome.runtime.getURL(`vendor/${name}/LICENSE`);
    fetch(license)
      .then(r => r.text())
      .then(text => {
        const code = document.createElement("code");
        code.textContent = text;
        pre.appendChild(code);
      });
  });

(Array.from(document.querySelectorAll("[data-external]")) as HTMLAnchorElement[])
  .forEach(a => {
    a.addEventListener("click", e => {
      e.preventDefault();
      chrome.tabs.create({active: true, url: a.href});
    });
  });
