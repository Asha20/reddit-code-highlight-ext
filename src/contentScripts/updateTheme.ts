import {getThemeURL} from "./index";
import {Theme} from "../common/themes";

/*
Reddit's Styled Components use selectors which have a higher specificity
  than the selectors used in Prism themes. Most notably, Reddit assigns the
  following properties to the <code> elements:

  .random-uid-one > .random-uid-two {
    background-color: transparent;
    color: rgb(34, 34, 34);
  }

  While Prism themes generally use selectors such as:

    code[class*="language-"]
    pre[class*="language-"]

  Since these selectors have a lower specificity than Reddit's selectors,
  Reddit will mess with Prism themes. This is solved in the following way:

    1. Use a <style> tag with fetched CSS instead of a <link>

    2. In the CSS, filter for selectors which apply to <code> and <pre>

    3. Further filter to only selectors which have the rules "color",
      "background" or "background-color". We only care about those rules
      since those are the ones that Reddit is interfering with.

    4. Reverse the rules, since sheet.insertRule(rule, 0) will insert the
      rule in the first spot and push every other rule down, effectively
      reversing the order of insertion.

    5. Insert the previously overriden Prism theme styles, but add !important
       to every one to make sure it overrides Reddit's styles.

  These steps are highlighted inside of the function itself as well.
*/
function ruleSelectorIncludesCodeOrPre(rule: CSSStyleRule) {
  return rule.selectorText.includes(`code[class*="language-"]`)
    || rule.selectorText.includes(`pre[class*="language-"]`);
}

function styleIncludesOneOf(styles: CSSStyleDeclaration, wanted: string[]) {
  return Array.from(styles).some(style => wanted.includes(style));
}

function injectCSS(url: string): Promise<void> {
  return new Promise((resolve, reject) => {
    const link = document.createElement("link");
    link.addEventListener("load", () => resolve());
    link.addEventListener("error", reject);
    link.rel = "stylesheet";
    link.href = url;
    (document.head || document.documentElement).appendChild(link);
  });
}

export function updateTheme(theme: Theme): Promise<HTMLStyleElement> {
  const previousTheme = document.querySelector(`.rch-prism-theme`);

  return fetch(getThemeURL(theme))
    .then(r => r.text())
    .then(css => {
      // [1]
      const styleEl = document.createElement("style");
      styleEl.textContent = css;
      styleEl.className += "rch-prism-theme";
      document.head.appendChild(styleEl);
      const sheet = styleEl.sheet as CSSStyleSheet;

      const overrideRules = (Array.from(sheet.cssRules) as CSSStyleRule[])
        .filter(rule => rule instanceof CSSStyleRule)
        // [2]
        .filter(ruleSelectorIncludesCodeOrPre)
        // [3]
        .filter(rule => styleIncludesOneOf(
          rule.style,
          ["color", "background-color", "background"],
        ))
        .map(rule => {
          const styles = rule.style;
          return {
            selector: rule.selectorText,
            color: styles.color,
            background: styles.background,
            backgroundColor: (styles as any)["background-color"],
          };
        })
        // [4]
        .reverse();

      // [5]
      overrideRules.forEach(styles => {
        const {selector, color, background, backgroundColor} = styles;
        sheet.insertRule(`${selector} {
          color: ${color} !important;
          background: ${background} !important;
          background-color: ${backgroundColor} !important;
        }`, 0);
      });

      styleEl.setAttribute("data-prism-theme", "true");

      if (previousTheme) {
        previousTheme.remove();
      }
      return styleEl;
    });
}
