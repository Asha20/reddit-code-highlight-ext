import {Dropdown} from "../common/dropdown";
import {languages, Language, PrismLanguage} from "../../data/languages";
import {getSuggestions} from "../../data/subreddits";
import {rehighlightElement} from "./index";

function languageDropdown(code: HTMLElement, selectedLanguageName: PrismLanguage) {
  const sortedLanguages = (Object.values(languages) as Language[])
    .sort((a, b) => a.name === "none" ? -1 : a.display.localeCompare(b.display));
  const dropdown = new Dropdown(sortedLanguages, "display");

  dropdown.setSelected(languages[selectedLanguageName]);
  return dropdown;
}

function suggestionButtons(subreddit: string) {
  const buttonHolder = document.createElement("div");
  buttonHolder.className += "rch-suggestion";
  const suggestions = getSuggestions(subreddit);

  suggestions.forEach(({name, display}) => {
    const button = document.createElement("button");
    button.className += "rch-suggestion__button";
    button.textContent = display;
    button.dataset.language = name;
    buttonHolder.appendChild(button);
  });
  return buttonHolder;
}

export function insertPicker(code: HTMLElement, subreddit: string, language: PrismLanguage) {
  const wrapper = document.createElement("div");
  wrapper.className += "rch-picker";

  const dropdown = languageDropdown(code, language);
  const suggestions = suggestionButtons(subreddit);

  const syncDropdownWithSuggestions = (lang: Language) => {
    const buttons = Array.from(suggestions.children) as HTMLButtonElement[];
    buttons.forEach(button => {
      button.disabled = button.dataset.language === lang.name;
    });
    rehighlightElement(code, lang);
  };

  dropdown.onChange(syncDropdownWithSuggestions);
  syncDropdownWithSuggestions(dropdown.getSelected());

  (Array.from(suggestions.children) as HTMLButtonElement[]).forEach(button => {
    button.addEventListener("click", () => {
      dropdown.setSelected(languages[button.dataset.language as PrismLanguage]);
    });
  });
  wrapper.appendChild(dropdown.getDOM());
  wrapper.appendChild(suggestions);
  const pre = code.parentElement!;
  code.style.marginTop = "0";
  pre.style.marginTop = "0";
  pre.parentElement!.insertBefore(wrapper, pre);
}
