import * as Prism from "prismjs";
import "prismjs/plugins/line-numbers/prism-line-numbers.js";
import {insertPicker} from "./picker";
import {languages, Language} from "../../data/languages";
import {determineLanguage} from "../../data/subreddits";
import {Theme} from "../common/themes";
import {updateTheme} from "./updateTheme";
import * as Storage from "../common/storage";

const sub = location.pathname.split("/")[2];

function importLanguage(language: Language) {
  if (Prism.languages[language.name] || language.name === "none") {
    return;
  }
  (language.dependencies || [])
    .forEach(dependency => importLanguage(languages[dependency]));

  require(`prismjs/components/prism-${language.name}.min.js`);
}

function highlightElement(code: HTMLElement) {
  const lang = determineLanguage(sub, code.textContent || "");
  code.className += ` language-${lang.name}`;
  const pre = code.parentElement!;
  pre.style.display = "block";
  pre.style.lineHeight = "1";
  pre.className += " line-numbers";
  insertPicker(code, sub, lang.name);
  importLanguage(lang);
  Prism.highlightElement(code);
  code.setAttribute("data-rch-highlighted", "true");
}

function highlightAllElements() {
  Array.from(document.getElementsByTagName("code"))
    .filter(code => !code.hasAttribute("data-rch-highlighted"))
    .filter(code => code.parentElement instanceof HTMLPreElement)
    .forEach(highlightElement);
}

export function rehighlightElement(code: HTMLElement, language: Language) {
  const content = code.textContent;
  Array.from(code.children).forEach(child => child.remove());
  importLanguage(language);
  code.className = code.className.replace(/language-.+/, "");
  code.classList.add(`language-${language.name}`);
  code.textContent = content;
  Prism.highlightElement(code);
}

export function getThemeURL(theme: Theme) {
  const filename = "prism" + (theme.name === "default" ? "" : "-" + theme.name) + ".css";
  return chrome.runtime.getURL(`vendor/prismjs/themes/${filename}`);
}

chrome.runtime.onMessage.addListener((request, sender) => {
  if (sender.id !== chrome.runtime.id) {
    return;
  }
  if (request.type === "UPDATE_THEME") {
    updateTheme(request.theme as Theme);
    Storage.setTheme(request.theme);
  }
});

setInterval(highlightAllElements, 1000);
Storage.getTheme().then(updateTheme);
