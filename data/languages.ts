// Obtained from https://prismjs.com/#Prismlanguages-list
export enum PrismLanguage {
  // Special value meant for unhighlighted code blocks
  none = "none",

  markup = "markup",
  css = "css",
  clike = "clike",
  javascript = "javascript",
  abap = "abap",
  actionscript = "actionscript",
  ada = "ada",
  apacheconf = "apacheconf",
  apl = "apl",
  applescript = "applescript",
  arduino = "arduino",
  arff = "arff",
  asciidoc = "asciidoc",
  asm6502 = "asm6502",
  aspnet = "aspnet",
  autohotkey = "autohotkey",
  autoit = "autoit",
  bash = "bash",
  basic = "basic",
  batch = "batch",
  bison = "bison",
  brainfuck = "brainfuck",
  bro = "bro",
  c = "c",
  csharp = "csharp",
  cpp = "cpp",
  coffeescript = "coffeescript",
  clojure = "clojure",
  crystal = "crystal",
  csp = "csp",
  "css-extras" = "css-extras",
  d = "d",
  dart = "dart",
  diff = "diff",
  django = "django",
  docker = "docker",
  eiffel = "eiffel",
  elixir = "elixir",
  elm = "elm",
  erb = "erb",
  erlang = "erlang",
  fsharp = "fsharp",
  flow = "flow",
  fortran = "fortran",
  gedcom = "gedcom",
  gherkin = "gherkin",
  git = "git",
  glsl = "glsl",
  go = "go",
  graphql = "graphql",
  groovy = "groovy",
  haml = "haml",
  handlebars = "handlebars",
  haskell = "haskell",
  haxe = "haxe",
  http = "http",
  hpkp = "hpkp",
  hsts = "hsts",
  ichigojam = "ichigojam",
  icon = "icon",
  inform7 = "inform7",
  ini = "ini",
  io = "io",
  j = "j",
  java = "java",
  jolie = "jolie",
  json = "json",
  julia = "julia",
  keyman = "keyman",
  kotlin = "kotlin",
  latex = "latex",
  less = "less",
  liquid = "liquid",
  lisp = "lisp",
  livescript = "livescript",
  lolcode = "lolcode",
  lua = "lua",
  makefile = "makefile",
  markdown = "markdown",
  "markup-templating" = "markup-templating",
  matlab = "matlab",
  mel = "mel",
  mizar = "mizar",
  monkey = "monkey",
  n4js = "n4js",
  nasm = "nasm",
  nginx = "nginx",
  nim = "nim",
  nix = "nix",
  nsis = "nsis",
  objectivec = "objectivec",
  ocaml = "ocaml",
  opencl = "opencl",
  oz = "oz",
  parigp = "parigp",
  parser = "parser",
  pascal = "pascal",
  perl = "perl",
  php = "php",
  "php-extras" = "php-extras",
  plsql = "plsql",
  powershell = "powershell",
  processing = "processing",
  prolog = "prolog",
  properties = "properties",
  protobuf = "protobuf",
  pug = "pug",
  puppet = "puppet",
  pure = "pure",
  python = "python",
  q = "q",
  qore = "qore",
  r = "r",
  jsx = "jsx",
  tsx = "tsx",
  renpy = "renpy",
  reason = "reason",
  rest = "rest",
  rip = "rip",
  roboconf = "roboconf",
  ruby = "ruby",
  rust = "rust",
  sas = "sas",
  sass = "sass",
  scss = "scss",
  scala = "scala",
  scheme = "scheme",
  smalltalk = "smalltalk",
  smarty = "smarty",
  sql = "sql",
  soy = "soy",
  stylus = "stylus",
  swift = "swift",
  tap = "tap",
  tcl = "tcl",
  textile = "textile",
  tt2 = "tt2",
  twig = "twig",
  typescript = "typescript",
  vbnet = "vbnet",
  velocity = "velocity",
  verilog = "verilog",
  vhdl = "vhdl",
  vim = "vim",
  "visual-basic" = "visual-basic",
  wasm = "wasm",
  wiki = "wiki",
  xeora = "xeora",
  xojo = "xojo",
  xquery = "xquery",
  yaml = "yaml",
}

export interface Language {
  name: PrismLanguage;
  display: string;
  dependencies?: PrismLanguage[];
}

const languagesArray: Language[] = [
  {
    name: PrismLanguage.none,
    display: "Plain Text",
  },
  {
    name: PrismLanguage.markup,
    display: "Markup",
  },
  {
    name: PrismLanguage.css,
    display: "CSS",
  },
  {
    name: PrismLanguage.clike,
    display: "C-like",
  },
  {
    name: PrismLanguage.javascript,
    display: "JavaScript",
  },
  {
    name: PrismLanguage.abap,
    display: "ABAP",
  },
  {
    name: PrismLanguage.actionscript,
    display: "ActionScript",
  },
  {
    name: PrismLanguage.ada,
    display: "Ada",
  },
  {
    name: PrismLanguage.apacheconf,
    display: "Apache Configuration",
  },
  {
    name: PrismLanguage.apl,
    display: "APL",
  },
  {
    name: PrismLanguage.applescript,
    display: "AppleScript",
  },
  {
    name: PrismLanguage.arduino,
    display: "Arduino",
    dependencies: [PrismLanguage.cpp],
  },
  {
    name: PrismLanguage.arff,
    display: "ARFF",
  },
  {
    name: PrismLanguage.asciidoc,
    display: "AsciiDoc",
  },
  {
    name: PrismLanguage.asm6502,
    display: "6502 Assembly",
  },
  {
    name: PrismLanguage.aspnet,
    display: "ASP.NET (C#)",
    dependencies: [PrismLanguage.csharp],
  },
  {
    name: PrismLanguage.autohotkey,
    display: "AutoHotkey",
  },
  {
    name: PrismLanguage.autoit,
    display: "AutoIt",
  },
  {
    name: PrismLanguage.bash,
    display: "Bash",
  },
  {
    name: PrismLanguage.basic,
    display: "BASIC",
  },
  {
    name: PrismLanguage.batch,
    display: "Batch",
  },
  {
    name: PrismLanguage.bison,
    display: "Bison",
    dependencies: [PrismLanguage.c],
  },
  {
    name: PrismLanguage.brainfuck,
    display: "Brainfuck",
  },
  {
    name: PrismLanguage.bro,
    display: "Bro",
  },
  {
    name: PrismLanguage.c,
    display: "C",
  },
  {
    name: PrismLanguage.csharp,
    display: "C#",
  },
  {
    name: PrismLanguage.cpp,
    display: "C++",
    dependencies: [PrismLanguage.c],
  },
  {
    name: PrismLanguage.coffeescript,
    display: "CoffeeScript",
  },
  {
    name: PrismLanguage.clojure,
    display: "Clojure",
  },
  {
    name: PrismLanguage.crystal,
    display: "Crystal",
    dependencies: [PrismLanguage.ruby],
  },
  {
    name: PrismLanguage.csp,
    display: "Content-Security-Policy",
  },
  {
    name: PrismLanguage["css-extras"],
    display: "CSS Extras",
  },
  {
    name: PrismLanguage.d,
    display: "D",
  },
  {
    name: PrismLanguage.dart,
    display: "Dart",
  },
  {
    name: PrismLanguage.diff,
    display: "Diff",
  },
  {
    name: PrismLanguage.django,
    display: "Django/Jinja2",
  },
  {
    name: PrismLanguage.docker,
    display: "Docker",
  },
  {
    name: PrismLanguage.eiffel,
    display: "Eiffel",
  },
  {
    name: PrismLanguage.elixir,
    display: "Elixir",
  },
  {
    name: PrismLanguage.elm,
    display: "Elm",
  },
  {
    name: PrismLanguage.erb,
    display: "ERB",
    dependencies: [PrismLanguage.ruby, PrismLanguage["markup-templating"]],
  },
  {
    name: PrismLanguage.erlang,
    display: "Erlang",
  },
  {
    name: PrismLanguage.fsharp,
    display: "F#",
  },
  {
    name: PrismLanguage.flow,
    display: "Flow",
  },
  {
    name: PrismLanguage.fortran,
    display: "Fortran",
  },
  {
    name: PrismLanguage.gedcom,
    display: "GEDCOM",
  },
  {
    name: PrismLanguage.gherkin,
    display: "Gherkin",
  },
  {
    name: PrismLanguage.git,
    display: "Git",
  },
  {
    name: PrismLanguage.glsl,
    display: "GLSL",
  },
  {
    name: PrismLanguage.go,
    display: "Go",
  },
  {
    name: PrismLanguage.graphql,
    display: "GraphQL",
  },
  {
    name: PrismLanguage.groovy,
    display: "Groovy",
  },
  {
    name: PrismLanguage.haml,
    display: "Haml",
    dependencies: [PrismLanguage.ruby],
  },
  {
    name: PrismLanguage.handlebars,
    display: "Handlebars",
    dependencies: [PrismLanguage["markup-templating"]],
  },
  {
    name: PrismLanguage.haskell,
    display: "Haskell",
  },
  {
    name: PrismLanguage.haxe,
    display: "Haxe",
  },
  {
    name: PrismLanguage.http,
    display: "HTTP",
  },
  {
    name: PrismLanguage.hpkp,
    display: "HTTP Public-Key-Pins",
  },
  {
    name: PrismLanguage.hsts,
    display: "HTTP Strict-Transport-Security",
  },
  {
    name: PrismLanguage.ichigojam,
    display: "IchigoJam",
  },
  {
    name: PrismLanguage.icon,
    display: "Icon",
  },
  {
    name: PrismLanguage.inform7,
    display: "Inform 7",
  },
  {
    name: PrismLanguage.ini,
    display: "Ini",
  },
  {
    name: PrismLanguage.io,
    display: "Io",
  },
  {
    name: PrismLanguage.j,
    display: "J",
  },
  {
    name: PrismLanguage.java,
    display: "Java",
  },
  {
    name: PrismLanguage.jolie,
    display: "Jolie",
  },
  {
    name: PrismLanguage.json,
    display: "JSON",
  },
  {
    name: PrismLanguage.julia,
    display: "Julia",
  },
  {
    name: PrismLanguage.keyman,
    display: "Keyman",
  },
  {
    name: PrismLanguage.kotlin,
    display: "Kotlin",
  },
  {
    name: PrismLanguage.latex,
    display: "LaTeX",
  },
  {
    name: PrismLanguage.less,
    display: "Less",
  },
  {
    name: PrismLanguage.liquid,
    display: "Liquid",
  },
  {
    name: PrismLanguage.lisp,
    display: "Lisp",
  },
  {
    name: PrismLanguage.livescript,
    display: "LiveScript",
  },
  {
    name: PrismLanguage.lolcode,
    display: "LOLCODE",
  },
  {
    name: PrismLanguage.lua,
    display: "Lua",
  },
  {
    name: PrismLanguage.makefile,
    display: "Makefile",
  },
  {
    name: PrismLanguage.markdown,
    display: "Markdown",
  },
  {
    name: PrismLanguage["markup-templating"],
    display: "Markup Templating",
  },
  {
    name: PrismLanguage.matlab,
    display: "MATLAB",
  },
  {
    name: PrismLanguage.mel,
    display: "MEL",
  },
  {
    name: PrismLanguage.mizar,
    display: "Mizar",
  },
  {
    name: PrismLanguage.monkey,
    display: "Monkey",
  },
  {
    name: PrismLanguage.n4js,
    display: "N4JS",
  },
  {
    name: PrismLanguage.nasm,
    display: "NASM",
  },
  {
    name: PrismLanguage.nginx,
    display: "nginx",
  },
  {
    name: PrismLanguage.nim,
    display: "Nim",
  },
  {
    name: PrismLanguage.nix,
    display: "Nix",
  },
  {
    name: PrismLanguage.nsis,
    display: "NSIS",
  },
  {
    name: PrismLanguage.objectivec,
    display: "Objective-C",
    dependencies: [PrismLanguage.c],
  },
  {
    name: PrismLanguage.ocaml,
    display: "OCaml",
  },
  {
    name: PrismLanguage.opencl,
    display: "OpenCL",
  },
  {
    name: PrismLanguage.oz,
    display: "Oz",
  },
  {
    name: PrismLanguage.parigp,
    display: "PARI/GP",
  },
  {
    name: PrismLanguage.parser,
    display: "Parser",
  },
  {
    name: PrismLanguage.pascal,
    display: "Pascal",
  },
  {
    name: PrismLanguage.perl,
    display: "Perl",
  },
  {
    name: PrismLanguage.php,
    display: "PHP",
    dependencies: [PrismLanguage["markup-templating"]],
  },
  {
    name: PrismLanguage["php-extras"],
    display: "PHP Extras",
  },
  {
    name: PrismLanguage.plsql,
    display: "PL/SQL",
    dependencies: [PrismLanguage.sql],
  },
  {
    name: PrismLanguage.powershell,
    display: "PowerShell",
  },
  {
    name: PrismLanguage.processing,
    display: "Processing",
  },
  {
    name: PrismLanguage.prolog,
    display: "Prolog",
  },
  {
    name: PrismLanguage.properties,
    display: ".properties",
  },
  {
    name: PrismLanguage.protobuf,
    display: "Protocol Buffers",
  },
  {
    name: PrismLanguage.pug,
    display: "Pug",
  },
  {
    name: PrismLanguage.puppet,
    display: "Puppet",
  },
  {
    name: PrismLanguage.pure,
    display: "Pure",
  },
  {
    name: PrismLanguage.python,
    display: "Python",
  },
  {
    name: PrismLanguage.q,
    display: "Q (kdb+ database)",
  },
  {
    name: PrismLanguage.qore,
    display: "Qore",
  },
  {
    name: PrismLanguage.r,
    display: "R",
  },
  {
    name: PrismLanguage.jsx,
    display: "React JSX",
  },
  {
    name: PrismLanguage.tsx,
    display: "React TSX",
    dependencies: [PrismLanguage.jsx, PrismLanguage.typescript],
  },
  {
    name: PrismLanguage.renpy,
    display: "Ren'py",
  },
  {
    name: PrismLanguage.reason,
    display: "Reason",
  },
  {
    name: PrismLanguage.rest,
    display: "reST (reStructuredText)",
  },
  {
    name: PrismLanguage.rip,
    display: "Rip",
  },
  {
    name: PrismLanguage.roboconf,
    display: "Roboconf",
  },
  {
    name: PrismLanguage.ruby,
    display: "Ruby",
  },
  {
    name: PrismLanguage.rust,
    display: "Rust",
  },
  {
    name: PrismLanguage.sas,
    display: "SAS",
  },
  {
    name: PrismLanguage.sass,
    display: "Sass (Sass)",
  },
  {
    name: PrismLanguage.scss,
    display: "Sass (Scss)",
  },
  {
    name: PrismLanguage.scala,
    display: "Scala",
    dependencies: [PrismLanguage.java],
  },
  {
    name: PrismLanguage.scheme,
    display: "Scheme",
  },
  {
    name: PrismLanguage.smalltalk,
    display: "Smalltalk",
  },
  {
    name: PrismLanguage.smarty,
    display: "Smarty",
    dependencies: [PrismLanguage["markup-templating"]],
  },
  {
    name: PrismLanguage.sql,
    display: "SQL",
  },
  {
    name: PrismLanguage.soy,
    display: "Soy (Closure Template)",
    dependencies: [PrismLanguage["markup-templating"]],
  },
  {
    name: PrismLanguage.stylus,
    display: "Stylus",
  },
  {
    name: PrismLanguage.swift,
    display: "Swift",
  },
  {
    name: PrismLanguage.tap,
    display: "TAP",
  },
  {
    name: PrismLanguage.tcl,
    display: "Tcl",
  },
  {
    name: PrismLanguage.textile,
    display: "Textile",
  },
  {
    name: PrismLanguage.tt2,
    display: "Template Toolkit 2",
    dependencies: [PrismLanguage["markup-templating"]],
  },
  {
    name: PrismLanguage.twig,
    display: "Twig",
  },
  {
    name: PrismLanguage.typescript,
    display: "TypeScript",
  },
  {
    name: PrismLanguage.vbnet,
    display: "VB.Net",
    dependencies: [PrismLanguage.basic],
  },
  {
    name: PrismLanguage.velocity,
    display: "Velocity",
  },
  {
    name: PrismLanguage.verilog,
    display: "Verilog",
  },
  {
    name: PrismLanguage.vhdl,
    display: "VHDL",
  },
  {
    name: PrismLanguage.vim,
    display: "vim",
  },
  {
    name: PrismLanguage["visual-basic"],
    display: "Visual Basic",
  },
  {
    name: PrismLanguage.wasm,
    display: "WebAssembly",
  },
  {
    name: PrismLanguage.wiki,
    display: "Wiki markup",
  },
  {
    name: PrismLanguage.xeora,
    display: "Xeora",
  },
  {
    name: PrismLanguage.xojo,
    display: "Xojo (REALbasic)",
  },
  {
    name: PrismLanguage.xquery,
    display: "XQuery",
  },
  {
    name: PrismLanguage.yaml,
    display: "YAML",
  },
];

export const languages = languagesArray
  .reduce((acc, x) => {
    acc[x.name] = x;
    return acc;
  }, {} as Record<PrismLanguage, Language>);
