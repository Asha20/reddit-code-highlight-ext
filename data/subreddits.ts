import * as Test from "./tests";
import {languages, Language} from "./languages";

interface SubredditGroup {
  subreddits: string[];
  tests: Test.LanguageTest[];
  suggestions: Language[];
  fallback?: Language;
}

const webdevTests: Test.LanguageTest[] = [Test.json, Test.javascript, Test.markup, Test.css];
const webdevSuggestions: Language[] = [
  languages.javascript,
  languages.css,
  languages.markup,
  languages.jsx,
  languages.typescript,
  languages.tsx,
];
function webdevGroup(subreddits: string[], fallback = languages.none): SubredditGroup {
  return {
    subreddits,
    tests: webdevTests,
    suggestions: webdevSuggestions,
    fallback,
  };
}

const subredditGroups: SubredditGroup[] = [
  webdevGroup(["javascript", "webdev", "frontend", "node", "html", "html5", "css"]),
  webdevGroup(["typescript"], languages.typescript),
  webdevGroup(["reactjs"], languages.jsx),
];

function extractLanguageName(subreddit: string) {
  const regexes = [
    /^learn(\w+)$/,
    /^(\w+)lang$/,
    /^(\w+)_language$/,
    /^(\w+)_questions$/,
  ];

  for (const regex of regexes) {
    const match = subreddit.match(regex);
    if (match) {
      return match[1].toLowerCase();
    }
  }
  return subreddit.toLowerCase();
}

export function determineLanguage(subreddit: string, code: string): Language {
  const extractedLanguage = extractLanguageName(subreddit);
  const subredditGroup = subredditGroups
    .find(group => group.subreddits.includes(extractedLanguage));

  if (!subredditGroup) {
    return (languages as any)[extractedLanguage] || languages.none;
  }

  for (const {language, predicate} of subredditGroup.tests) {
    if (predicate(code)) {
      return language;
    }
  }
  return subredditGroup.fallback || languages.none;
}

export function getSuggestions(subreddit: string) {
  const extractedLanguage = extractLanguageName(subreddit);
  const subredditGroup = subredditGroups
    .find(group => group.subreddits.includes(extractedLanguage));

  return subredditGroup ? subredditGroup.suggestions : [];
}
