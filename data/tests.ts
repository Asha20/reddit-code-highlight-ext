import {languages, Language} from "./languages";

export interface LanguageTest {
  language: Language;
  predicate: (code: string) => boolean;
}

export const javascript = {
  language: languages.javascript,
  predicate: function testJavascript(code: string) {
    try {
      // tslint:disable-next-line:no-unused-expression
      new Function(code);
      return true;
    } catch (e) {
      return false;
    }
  },
};

export const markup = {
  language: languages.markup,
  predicate: function testMarkup(code: string) {
    const numberOfLines = code
      .split("\n")
      .filter(line => line.trim().length > 0)
      .length;

    const leftArrowCount = (code.match(/</g) || []).length;
    const rightArrowCount = (code.match(/[^=]>/g) || []).length;

    return (leftArrowCount + rightArrowCount) / numberOfLines > 1;
  },
};

export const  css = {
  language: languages.css,
  predicate: function testCss(code: string) {
    const style = document.createElement("style");
    // The <style> needs to be appended to the document in order to access the sheet,
    // but since we don't want to potentially apply random styles on the page, we
    // use a media query to prevent it from styling the page.
    style.setAttribute("media", "max-width: 1px");
    style.textContent = code;
    document.head.appendChild(style);

    const cssRules = Array.from((style.sheet as CSSStyleSheet).cssRules);
    const result = cssRules.length > 0 && cssRules
      .every(rule => (rule as any).style.length > 0);

    style.remove();
    return result;
  },
};

export const json = {
  language: languages.json,
  predicate: function testJson(code: string) {
    try {
      // tslint:disable-next-line:no-unused-expression
      JSON.parse(code);
      return true;
    } catch (e) {
      return false;
    }
  },
};
