const webpack = require("webpack");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const path = require("path");
const merge = require("webpack-merge");

const {NODE_ENV = "development"} = process.env;

const base = {
  context: __dirname,
  resolve: {
    extensions: [".ts", ".js"],
  },
  entry: {
    "contentScripts/index": "./src/contentScripts/index.ts",
    "popup/index": "./src/popup/index.ts",
    "popup/attributions": "./src/popup/attributions.ts",
  },
  output: {
    path: path.join(__dirname, "dist"),
    filename: "[name].js",
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        loader: "ts-loader",
      },
    ],
  },
  plugins: [
    new CopyWebpackPlugin([
      {from: "./src/manifest.json", to: "./manifest.json"},
      {from: "./src/vendor", to: "vendor"},
      {from: "./src/icons", to: "icons"},
      {context: "./src", from: "**/*.html", to: path.resolve(__dirname, "dist")},
      {context: "./src", from: "**/*.css", to: path.resolve(__dirname, "dist"), ignore: ["vendor/**"]},
    ]),
    new webpack.DefinePlugin({
      NODE_ENV: JSON.stringify(NODE_ENV),
    }),
    new webpack.WatchIgnorePlugin([
      /\.js$/,
      /\.d\.ts$/,
    ]),
  ],
};

const development = merge(base, {
  mode: "development",
  devtool: "inline-source-map",
});

const production = merge(base, {
  mode: "production",
  devtool: "#source-map",
  plugins: [
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false,
    }),
  ],
});

module.exports = NODE_ENV === "development"
  ? development
  : production;
