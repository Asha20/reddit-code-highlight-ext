# Contributing

## Installing

Downloading the most recent public release:

    git clone https://gitlab.com/Asha20/reddit-code-highlight.git

Downloading the most recent development version:

    git clone https://gitlab.com/Asha20/reddit-code-highlight.git -b develop --single-branch

After downloading run `npm run install` to install the dependencies. Use `npm run watch` while developing, and `npm run build` to create the production build. The built extension files will be inside the `dist` folder.

## Versioning

 - Increment the *patch* version when:
    - Modifying files such as `README.md`, `CONTRIBUTING.md`...
    - Fixing a typo.
    - Introducing a bug fix.
 - Increment the *minor* version when:
    - Adding a new feature.
    - Making a small change to the UI.
    - Introducing additional languages to the language detection system.
 - Increment the *major* version when the UI is significantly changed.

## Branch naming

When creating a new pull request, branch off from `develop` and let your branch have the `pr/` prefix, followed by the kebab-case branch name, `pr/like-so`.

## Improving the automatic language detection feature

Inside of [`data/subreddits.ts`](data/subreddits.ts), there is a variable called `subredditGroups`. This variable contains all of the data which the extension uses when guessing the language inside of a code block. If you want to add a language test to use in a `SubredditGroup`, put it inside of [`data/tests.ts`](data/tests.ts). I've only included tests for front-end languages since they weren't hard to implement.

If you're planning to add language tests, make sure all of the testing is done client-side; don't send requests to external validation APIs.

After adding more subreddit groups, increment the *minor* version and send the pull request.

## Fixing bugs, adding features...
 
There's nothing special to say here. Just make sure you increment the version number properly according to [Versioning](#Versioning).